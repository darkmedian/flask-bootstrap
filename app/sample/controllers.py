
from flask import Flask, render_template, Blueprint, jsonify
from app.functions import support_jsonp
from datetime import datetime
# Import the database object from the main app module
from app import db
from app.sample.models import Sample


def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

# Define the blueprint: 'auth', set its url prefix: app.url/auth
sample = Blueprint('sample', __name__, url_prefix='/sample')


@sample.route('/', methods=['GET'])
@support_jsonp
def index():
    print db
    records = Sample.query.all()
    results = []
    # for record in records:
    #     results.append(
    #         {'log_date': record.log_date, 'log_title': record.log_title})
    # response = jsonify(response=results)
    # response.status_code = 200
    # return response
    return "I got nothing for ya!"