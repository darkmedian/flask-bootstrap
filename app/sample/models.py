# Import the database object (db) from the main application module
# We will define this inside /app/__init__.py in the next sections.
from app import db

# Define a base model for other database tables to inherit


class Base(db.Model):

    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime,  default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime,  default=db.func.current_timestamp(),
                              onupdate=db.func.current_timestamp())


# Define a User model


class Sample(Base):

    __tablename__ = 'sample'
    title = db.Column(db.String(256), nullable=False)

    # New instance instantiation procedure
    def __init__(self, title):
        self.log_date = title

    def __repr__(self):
        return '<Sample %s, %s, %s>' % (self.title)



