from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:postgres@localhost/logs'

from app.sample.controllers import sample as sample_module
app.register_blueprint(sample_module, template_folder='templates')

db.create_all()


def create_app(config=None, app_name=None, register_blueprints=True):
    """
    Create Flask app
    """
    app = Flask(app_name)
    configure_app(app, config)

    # So no circular import when using Celery
    if register_blueprints:
        # move the import into configure_blueprints
        from .module_one import module_one
        configure_blueprints(app)

    configure_extensions(app)
    configure_error_handlers(app)

    return app

