import json
import time
from functools import wraps
from flask import redirect, request, current_app
from flask import url_for as _url_for, _request_ctx_stack
import chardet


def decode(data, encoding=None):
    if isinstance(data, unicode):
        return data
    if encoding:
        return data.decode(encoding)
    try:
        return data.decode('utf-8')
    except UnicodeDecodeError:
        encoding = chardet.detect(data)['encoding']
        if not encoding:
            return "(Binary data)"
        return data.decode(encoding)


class memoize:
    def __init__(self, function):
        self.function = function
        self.memoized = {}

    def __call__(self, *args):
        args_ = args
        if args and hasattr(args[0], 'path'):
            args_ = (args[0].path,) + args[1:]
        try:
            return self.memoized[args_]
        except KeyError:
            self.memoized[args_] = self.function(*args)
        return self.memoized[args_]


def support_jsonp(f):
    """Wraps JSONified output for JSONP """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        callback = request.args.get('callback', False)
        if callback:
            content = str(callback) + '(' + str(f(*args, **kwargs).data) + ')'
            return current_app.response_class(content, mimetype='application/javascript')
        else:
            return f(*args, **kwargs)
    return decorated_function


def humantime(ctime):
    timediff = time.time() - ctime
    if timediff < 0:
        return 'in the future'
    if timediff < 60:
        return 'just now'
    if timediff < 120:
        return 'a minute ago'
    if timediff < 3600:
        return "%d minutes ago" % (timediff / 60)
    if timediff < 7200:
        return "an hour ago"
    if timediff < 86400:
        return "%d hours ago" % (timediff / 3600)
    if timediff < 172800:
        return "a day ago"
    if timediff < 2592000:
        return "%d days ago" % (timediff / 86400)
    if timediff < 5184000:
        return "a month ago"
    if timediff < 31104000:
        return "%d months ago" % (timediff / 2592000)
    if timediff < 62208000:
        return "a year ago"
    return "%d years ago" % (timediff / 31104000)


def timestamp():
    """Return the current timestamp as an integer."""
    return int(time.time())


def url_for(*args, **kwargs):
    """
    url_for replacement that works even when there is no request context.
    """
    if '_external' not in kwargs:
        kwargs['_external'] = False
    reqctx = _request_ctx_stack.top
    if reqctx is None:
        if kwargs['_external']:
            raise RuntimeError('Cannot generate external URLs without a '
                               'request context.')
        with current_app.test_request_context():
            return _url_for(*args, **kwargs)
    return _url_for(*args, **kwargs)